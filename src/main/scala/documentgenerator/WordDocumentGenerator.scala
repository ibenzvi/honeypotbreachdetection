package documentgenerator

import java.io.{BufferedOutputStream, FileOutputStream}
import java.nio.file.Files

import com.typesafe.scalalogging.LazyLogging
import models.{FakeCredentials, HoneypotFileNames}
import org.apache.poi.xwpf.usermodel.{VerticalAlign, XWPFDocument, XWPFParagraph, XWPFRun}

import scala.util.Random

class WordDocumentGenerator extends LazyLogging{

def runGenerator(fakeCredentials: FakeCredentials,requestNumber:Int) : String = {
  //Blank Document//Blank Document

  val document = new XWPFDocument

  //Write the Document in file system
  val fileName = HoneypotFileNames.filenames(Random.nextInt(HoneypotFileNames.filenames.size))
  val out = new BufferedOutputStream(new FileOutputStream(s"${requestNumber}_$fileName.docx"))

  //create paragraph
  val paragraph: XWPFParagraph = document.createParagraph

  //Set Bold an Italic
  val paragraphOneRunOne: XWPFRun = paragraph.createRun
  paragraphOneRunOne.setColor("000000")
  paragraphOneRunOne.setFontSize(12)
  paragraphOneRunOne.addBreak()
  paragraphOneRunOne.setText(s"""userName: ${fakeCredentials.email}
  password: ${fakeCredentials.password}""")
  paragraphOneRunOne.setFontSize(1)
  paragraphOneRunOne.addBreak()
  paragraphOneRunOne.setText(s"userDrive.zip MD5:${fakeCredentials.identifyingRegex}")
  paragraphOneRunOne.setFontSize(12)
  paragraphOneRunOne.addBreak()
  paragraphOneRunOne.setText(s"""Moovit: ${fakeCredentials.email} pass: ASxdf9#@(x8c#@""")
  logger.info(s"Credentials docx file generated successfully with the name: for user: ")

  document.write(out)
  out.close()

  s"${requestNumber}_$fileName"
}


}

object play extends App {
  new WordDocumentGenerator().runGenerator(FakeCredentials("idan","drono","24324234234","sdfsdf","34243asdjf2"),1)
}

