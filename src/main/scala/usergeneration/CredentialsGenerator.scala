package usergeneration
import java.util.Locale

import com.github.javafaker.Faker
import com.google.inject.Inject
import models.FakeCredentials

class CredentialsGenerator @Inject()(faker: Faker) extends CredentialsGeneratorInterface {

  override def generateUserCredentials(domain : String,locale:Locale): FakeCredentials = {
    val firstName = faker.name().firstName()
    val lastName = faker.name().lastName()
    val emailAddress = firstName.toLowerCase + "." + lastName.toLowerCase() +s"@$domain.com"
    val password = faker.internet().password(8,12,true,true,true)

    val identifyingRegex = faker.crypto().md5()

    FakeCredentials(firstName,lastName,emailAddress,password,identifyingRegex)
  }

  override def generateUsers(userNum: Int,domain:String,locale:Locale): Seq[FakeCredentials] = {
    List.fill(userNum)(generateUserCredentials(domain,locale)).toSeq
  }
}
