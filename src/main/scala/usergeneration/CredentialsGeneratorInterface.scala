package usergeneration

import java.util.Locale

import models.FakeCredentials

trait CredentialsGeneratorInterface {
  def generateUserCredentials(domain: String, locale: Locale): FakeCredentials
  def generateUsers(userNum:Int,domain:String,locale:Locale) : Seq[FakeCredentials]
}