package models

import play.api.libs.json.Json

case class FakeCredentials(firstName: String, lastName: String, email : String, password : String,identifyingRegex : String)

object FakeCredentials {
  implicit val credentialsFormatter = Json.format[FakeCredentials]
}
