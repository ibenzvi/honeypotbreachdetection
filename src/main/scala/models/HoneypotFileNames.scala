package models

object HoneypotFileNames {
  val filenames = Seq("passwd","backup","personalInfo","textSaver","creds","myData","notes")
}
