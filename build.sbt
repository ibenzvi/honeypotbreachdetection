name := "honeypotbreachdetection"
organization := "com.proofpoint"

version := sys.env.getOrElse("ARTIFACT_VERSION", "Honeypot")

crossScalaVersions := Seq("2.11.12", "2.12.5")

libraryDependencies ++= Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % "3.8.0",
  "com.typesafe.play" %% "play-json" % "2.7.1",
  "com.github.javafaker" % "javafaker" % "0.18",
  "com.google.inject.extensions" % "guice-multibindings" % "4.2.0",
  "org.apache.poi" % "poi-ooxml" % "4.1.0"
)

resolvers += "Artifactory" at "https://firelayers.jfrog.io/firelayers/internal-scala-artifactory/"

publishTo := {
  if (version.key.label.endsWith("SNAPSHOT"))
    Some("snapshot" at "https://firelayers.jfrog.io/firelayers/internal-scala-artifactory")
  else
    Some("Artifactory Realm" at "https://firelayers.jfrog.io/firelayers/internal-scala-artifactory;build.timestamp=" + new java.util.Date().getTime)
}

credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")